" Vim config file
"  https://vimawesome.com/
"  https://www.freecodecamp.org/news/vim-for-people-who-use-visual-studio-code/
"  https://github.com/ryanoasis/nerd-fonts#patched-fonts
"  https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/JetBrainsMono

"" Autoload  junegunn / vim-plug 
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
                \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')           " Plugins will be downloaded under the specified directory
Plug 'mhinz/vim-startify'                       " A start screen for Vim
Plug '907th/vim-auto-save'                      " Automatically saves changes to disk
Plug 'vim-airline/vim-airline'                  " Airline (bar) plugin
Plug 'vim-airline/vim-airline-themes'           " Airline (themes) plugin
Plug 'ryanoasis/vim-devicons'                   " This will add icons to stuff like NERDTree
Plug 'ayu-theme/ayu-vim'                        " Ayu.vim theme
Plug 'tomasr/molokai'                           " Molokai theme
Plug 'BurntSushi/ripgrep'                       " Recursively searches your current directory for a regex pattern
Plug 'junegunn/fzf'                             " General-purpose command-line fuzzy finder
Plug 'scrooloose/syntastic'                     " Syntax checking plugin
Plug 'luochen1990/rainbow'                      " Show different levels of parentheses in different colors
Plug 'scrooloose/nerdtree'                      " A tree explorer plugin for vim
Plug 'Xuyuanp/nerdtree-git-plugin'              " NERDTree showing git status flags
Plug 'tpope/vim-fugitive'                       " A Git wrapper so awesome, it should be illegal
Plug 'jiangmiao/auto-pairs'                     " Autocomplete Brackets
Plug 'nathanaelkane/vim-indent-guides'          " Visually displaying indent levels in Vim
Plug 'prettier/vim-prettier'                    " A vim plugin wrapper for prettier
Plug 'pangloss/vim-javascript'                  " JavaScript support
Plug 'leafgarland/typescript-vim'               " TypeScript syntax
Plug 'maxmellon/vim-jsx-pretty'                 " JS and JSX syntax
Plug 'rickhowe/spotdiff.vim'                    " A range selectable diffthis to compare partially
Plug 'jparise/vim-graphql'                      " GraphQL syntax
Plug 'neoclide/coc.nvim', 
    \ {'branch': 'release'}                     " Conquer of Completion
Plug 'iamcco/markdown-preview.nvim',
    \  {'do': 'cd app & yarn install'}          " Markdown Preview for (Neo)vim
call plug#end()                             " List ends here. Plugins become visible to Vim after this call.

" Vim globals
set vb t_vb=         " Disable beep and flash with gvimrc
set expandtab        " Indent (remember gg =G)
set shiftwidth=4
set smarttab
set encoding=UTF-8   " Encoding for Nerd fonts
set number           " Show line number
set signcolumn=yes   " Always show the sign
set autochdir
syntax on
syntax sync minlines=10000
syntax sync fromstart

" Create Backups/Swap/Undo folders
if empty(glob('~/.vim/tmp/README'))
    silent !mkdir -p ~/.vim/tmp
    silent !echo "Backup/Swap/Undo folders" | tee ~/.vim/tmp/README
    silent !mkdir -p ~/.vim/tmp/backup
    silent !mkdir -p ~/.vim/tmp/swap
    silent !mkdir -p ~/.vim/tmp/undo
endif

" Double trailing to avoids name collisions
set backupdir=~/.vim/tmp/backup//
set directory=~/.vim/tmp/swap//
set undodir=~/.vim/tmp/undo//

" Jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" GUI/Terminal config
if has('gui_running')
    set guifont=FiraCodeNerdFontCompleteM-Regular:h12
    set termguicolors     " enable true colors support
    let ayucolor="light"  " for light version of theme
    "let ayucolor="mirage" " for mirage version of theme
    "let ayucolor="dark"   " for dark version of theme
    colorscheme ayu
else
    if $TERM == 'screen'
        "let g:rehash256 = 1   " calculate theme in 256 palette
        set term=screen-256color
    else
        "set termguicolors     " enable true colors support
    endif
    colorscheme molokai
endif

" Syntastic config for statusline
if !empty(glob('~/.vim/plugged/syntastic/autoload/syntastic/util.vim'))
    set statusline+=%#warningmsg#
    set statusline+=%{SyntasticStatuslineFlag()}
    set statusline+=%*
endif

" CoC install the missing extensions for you on server start
" [TypeScript support, YAML support,  Markdown support, CoC Spelling Check, CoC Spelling Dicts]
let g:coc_global_extensions = ['coc-tsserver', 'coc-yaml', 'coc-markdownlint', 'coc-spell-checker', 'coc-cspell-dicts']

" CoC Spell config
if empty(glob('~/.vim/plugged/coc.nvim/autoload/coc.vim'))
    "cSpellExt.enableDictionaries": ["english"]
    "cSpellExt.enableDictionaries": ["spanish"]
    "cSpell.language": "en,es",
endif

" Plugins config
let g:airline#extensions#tabline#enabled = 1      " Smarter tab line
let g:airline_powerline_fonts = 1                 " Integrating with powerline fonts
let g:webdevicons_enable_nerdtree = 1             " Adding the flags to NERDTree
let g:webdevicons_enable_airline_tabline = 1      " Adding to vim-airline's tabline
let g:webdevicons_enable_airline_statusline = 1   " Adding to vim-airline's statusline
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1 " Force extra padding in NERDTree so that the filetype icons line up vertically
let g:webdevicons_enable_startify = 1             " Adding to vim-startify screen
let g:syntastic_always_populate_loc_list = 1      " Syntastic recommended settings
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:rainbow_active = 0                          " Rainbow parentheses disabled
let g:mkdp_browser = 'firefox'
let g:mkdp_auto_start = 0

" To do
" - Sign icon plugin (see :help sign)
set t_RV=⌚